import { Suspense } from 'react';
import { createRoot } from 'react-dom/client';
import { BrowserRouter as Router, useRoutes } from 'react-router-dom';
import { ConfigProvider } from 'antd';

// 国际化
import 'dayjs/locale/en';
import 'dayjs/locale/zh-cn';
// import enUS from 'antd/locale/en_US';
import zhCN from 'antd/es/locale/zh_CN';

import './styles/globals.scss';
import '@fa/theme/theme.scss';
import '@fa/ui/styles.css';
import { PageLoading } from '@fa/ui';

import routes from '~react-pages';

// eslint-disable-next-line no-console
console.log(routes);

import { each, trim } from 'lodash';

function loop(path: string, rs: any[]): any[] {
  const list: any[] = [];
  each(rs, (r) => {
    if (trim(r.path) !== '') {
      list.push(path + '/' + r.path);
      if (r.children) {
        list.push(...loop(path + '/' + r.path, r.children));
      }
    }
  });
  return list;
}

const flatRoutes = loop('', routes);
console.log('flatRoutes', flatRoutes);
// @ts-ignore
window.flatRoutes = flatRoutes;

function App() {
  return <Suspense fallback={<PageLoading />}>{useRoutes(routes)}</Suspense>;
}

const app = createRoot(document.getElementById('app')!);

app.render(
  <Router>
    <ConfigProvider locale={zhCN}>
      <App />
    </ConfigProvider>
  </Router>,
);
