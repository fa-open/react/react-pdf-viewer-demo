import React from 'react';
import { Link } from 'react-router-dom';

/**
 * @author xu.pengfei
 * @date 2022/12/17 17:23
 */
export default function index() {
  return (
    <div style={{ padding: 24 }}>
      {window.flatRoutes
        .filter((i: any) => i.startsWith('/pdf/'))
        .map((i: any) => (
          <div key={i} style={{ padding: 2 }}>
            <Link to={i}>{i}</Link>
          </div>
        ))}
    </div>
  );
}
